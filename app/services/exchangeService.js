(function() {
    var exchangeService = function($http) {

        this.getRates = function() {
            // return rates;
            var exchangeRateUrl = "https://api.exchangeratesapi.io/latest"
            return $http.get(exchangeRateUrl);
        }


    };

    angular.module('exchangeApp')
        .service('exchangeService', exchangeService);
}());