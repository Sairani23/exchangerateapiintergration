(function() {

    var newController = function($http, $scope) {

        $scope.amount = 1;
        $scope.amounts = 1;

        $http.get("https://api.exchangeratesapi.io/latest")
            .success(function(response) {

                $scope.rates = response.rates;
                $scope.base = response.base;
                $scope.date = response.date;

            });

    }

    // newController.$inject = ['$scope', '$http'];

    angular.module('exchangeApp')
        .controller('newController', newController);

}())