(function() {

    var ExchangeController = function($scope, exchangeService) {
        $scope.keys = [];

        function init() {

            exchangeService.getRates()
                .success(function(result) {

                    $scope.rates = result.rates;
                    $scope.date = result.date;
                    $scope.base = result.base;
                })
        }
        $scope.selectedItemChanged = function(keyValue) {

            $scope.CurrencyValue = $scope.rates[keyValue];
        }

        init();

    };

    ExchangeController.$inject = ['$scope', 'exchangeService'];

    angular.module('exchangeApp')
        .controller('ExchangeController', ExchangeController);

}());